<?php

/**
 * Start cleanup functions
 * ----------------------------------------------------------------------------
 */

if ( ! function_exists( 'dfri_start_cleanup' ) ) :
function dfri_start_cleanup() {

	// Launching operation cleanup
	add_action( 'init', 'dfri_cleanup_head' );

	// Remove WP version from RSS
	add_filter( 'the_generator', 'dfri_remove_rss_version' );

	// Remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'dfri_remove_wp_widget_recent_comments_style', 1 );

	// Clean up comment styles in the head
	add_action( 'wp_head', 'dfri_remove_recent_comments_style', 1 );

	// Clean up gallery output in wp
	add_filter( 'dfri_gallery_style', 'dfri_gallery_style' );

	// Additional post related cleaning
	add_filter( 'get_dfri_image_tag_class', 'dfri_image_tag_class', 0, 4 );
	add_filter( 'get_image_tag', 'dfri_image_editor', 0, 4 );
	add_filter( 'the_content', 'img_unautop', 30 );

	/**
	 * A few privacy-enhancing measures
	 */

	// Don't log IP addresses of comment authors
	add_filter( 'pre_comment_user_ip', '__return_zero' );

	// Don't set comment author cookies
	remove_action( 'set_comment_cookies', 'wp_set_comment_cookies' );

}
add_action( 'after_setup_theme','dfri_start_cleanup' );
endif;
/**
 * Clean up head
 * ----------------------------------------------------------------------------
 */

if ( ! function_exists( 'dfri_cleanup_head' ) ) :
function dfri_cleanup_head() {

	// Windows Live Writer
	remove_action( 'wp_head', 'wlwmanifest_link' );

	// WP version
	remove_action( 'wp_head', 'wp_generator' );

	// Prevent unnecessary info from being displayed
	add_filter( 'login_errors', create_function( '$a', 'return null;' ) );

}
endif;

// remove WP version from RSS
if ( ! function_exists( 'dfri_remove_rss_version' ) ) :
function dfri_remove_rss_version() {
	return '';
}
endif;

// remove injected CSS for recent comments widget
if ( ! function_exists( 'dfri_remove_wp_widget_recent_comments_style' ) ) :
function dfri_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}
endif;

// remove injected CSS from recent comments widget
if ( ! function_exists( 'dfri_remove_recent_comments_style' ) ) :
function dfri_remove_recent_comments_style() {
	global $wp_widget_factory;
	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
	}
}
endif;

// remove injected CSS from gallery
if ( ! function_exists( 'dfri_gallery_style' ) ) :
function dfri_gallery_style( $css ) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}
endif;

/**
 * Clean up image tags
 * ----------------------------------------------------------------------------
 */

// Remove default inline style of wp-caption

if ( ! function_exists( 'dfri_fixed_img_caption_shortcode' ) ) :
add_shortcode( 'wp_caption', 'dfri_fixed_img_caption_shortcode' );
add_shortcode( 'caption', 'dfri_fixed_img_caption_shortcode' );
function dfri_fixed_img_caption_shortcode( $attr, $content = null ) {
	if ( ! isset( $attr['caption'] ) ) {
		if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content = $matches[1];
			$attr['caption'] = trim( $matches[2] );
		}
	}

	$output = apply_filters( 'img_caption_shortcode', '', $attr, $content );

	if ( '' !== $output ) {
		return $output;
	}

	extract( shortcode_atts( array(
		'id'    => '',
		'align' => 'alignnone',
		'width' => '',
		'caption' => '',
		'class'   => '',
	), $attr) );

	if ( 1 > (int) $width || empty($caption) ) {
		return $content;
	}

	$markup = '<figure';
	if ( $id ) { $markup .= ' id="' . esc_attr( $id ) . '"'; }
	if ( $class ) { $markup .= ' class="' . esc_attr( $class ) . '"'; }
	$markup .= '>';
	$markup .= do_shortcode( $content ) . '<figcaption>' . $caption . '</figcaption></figure>';
	return $markup;
}
endif;

// Clean the output of attributes of images in editor
if ( ! function_exists( 'dfri_image_tag_class' ) ) :
function dfri_image_tag_class( $class, $id, $align, $size ) {
	$align = 'align' . esc_attr( $align );
	return $align;
}
endif;

// Remove width and height in editor, for a better responsive world.
if ( ! function_exists( 'dfri_image_editor' ) ) :
function dfri_image_editor($html, $id, $alt, $title) {
	return preg_replace( array(
			'/\s+width="\d+"/i',
			'/\s+height="\d+"/i',
			'/alt=""/i',
		),
		array(
			'',
			'',
			'',
			'alt="' . $title . '"',
		),
		$html
	);
}
endif;

// Wrap images with figure tag - Credit: Robert O'Rourke - http://bit.ly/1q0WHFs
if ( ! function_exists( 'img_unauto' ) ) :
function img_unautop( $pee ) {
	$pee = preg_replace( '/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<figure>$1</figure>', $pee );
	return $pee;
}
endif;

if ( ! function_exists( 'dfri_set_tiny_mce_block_formats' ) ) :
// Specify formats available in the format dropdown on the post edit page.
// The only purpose of this function is to disable h1.
function dfri_set_tiny_mce_block_formats( $arr ) {
	$arr['block_formats'] = 'Paragraph=p;Pre=pre;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
	return $arr;
}
endif;
add_filter( 'tiny_mce_before_init', 'dfri_set_tiny_mce_block_formats' );

?>
