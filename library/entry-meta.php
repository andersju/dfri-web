<?php
if ( ! function_exists( 'dfri_entry_meta' ) ) :
	function dfri_entry_meta() {
		echo '<time class="updated" datetime="' . esc_attr( get_the_time( 'c' ) ) . '">' . esc_html( get_the_date() ) . '</time>';
		echo ' &middot; ';
		echo '<span class="byline author"><a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" rel="author" class="fn">' . get_the_author() . '</a></span>';
	}
endif;
?>
