<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
		<meta name="referrer" content="never" />
	</head>
	<body <?php body_class(); ?>>

	<div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">

	<header id="top-header" class="show-for-medium-up contain-to-grid">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php bloginfo( 'name' ); ?>
					</a>
				</h1>
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description ) : ?>
				    <h2 class="site-description"><?php echo esc_html( $description ); ?></h2>
				<?php endif; ?>
			</div>
		</div>
	</header>

	<nav class="tab-bar show-for-small-only">
		<section class="left-small">
			<a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
		</section>
		<section class="middle tab-bar-section">

			<h1 class="title"><?php bloginfo( 'name' ); ?></h1>

		</section>
	</nav>

	<?php get_template_part( 'parts/off-canvas-menu' ); ?>

	<?php get_template_part( 'parts/top-bar' ); ?>

<section class="container" role="document">
