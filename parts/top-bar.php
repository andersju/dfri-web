<div class="top-bar-container contain-to-grid show-for-medium-up">
    <nav class="top-bar" data-topbar role="navigation">
        <section class="top-bar-section">
            <?php dfri_top_bar_l(); ?>
            <?php dfri_top_bar_r(); ?>
        </section>
        <div class="top-bar-search-form show-for-large-up">
                <?php get_search_form( true ); ?>
        </div>
       
    </nav>
</div>
