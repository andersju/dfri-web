<span class="post-meta">
    <time class="updated" datetime="<?php echo esc_attr( get_the_time( 'c' ) ); ?>">
        <?php echo get_the_date(); ?>
    </time>
     &middot; 
    <span class="byline author">
        <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author" class="fn">
            <?php echo get_the_author(); ?>
        </a>
    </span>
    <?php
	if ( comments_open() || pings_open() ) {
		$num_comments = get_comments_number();

		if ( 1 == $num_comments ) {
			$comment_icon = '<i class="fa fa-comment-o"></i>';
		} elseif ( $num_comments > 1 ) {
			$comment_icon = '<i class="fa fa-comments-o"></i>';
		}

		if ( $num_comments ) {
			$comment_meta = $comment_icon . ' ' . $num_comments;
		?>
			<span class="post-meta-comments"> &middot; 
				<a href="<?php comments_link(); ?>">
					<?php echo $comment_meta; ?>
				</a>
			</span>
			<?php
		}
	}
	?>
</span>