<?php
$tag = get_the_tags();

if ( $tag ) {
	$num_tags = count( get_the_tags() );
	if ( 1 == $num_tags ) {
		echo '<i class="fa fa-tag"></i> ';
	} else {
		echo '<i class="fa fa-tags"></i> ';
	}
	the_tags( '', ', ', '' );
}
