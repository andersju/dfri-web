<?php
if ( have_comments() ) :
	if ( (is_page() || is_single()) && ( ! is_home() && ! is_front_page()) ) :
?>
	<section id="comments"><?php

		wp_list_comments(
			array(
				'walker'            => new Dfri_Comments(),
				'max_depth'         => '',
				'style'             => 'ol',
				'callback'          => null,
				'end-callback'      => null,
				'type'              => 'all',
				'reply_text'        => esc_html__( 'Reply', 'dfri' ),
				'page'              => '',
				'per_page'          => '',
				'avatar_size'       => 48,
				'reverse_top_level' => null,
				'reverse_children'  => '',
				'format'            => 'html5',
				'short_ping'        => false,
				'echo'  	    => true,
				'moderation' 	    => esc_html__( 'Your comment is awaiting moderation.', 'dfri' ),
			)
		);

		?>

 	</section>
<?php
	endif;
endif;
?>

<?php
// Do not delete these lines

// Prevent access to this file directly
defined( 'ABSPATH' ) or die( esc_html__( 'Please do not load this page directly. Thanks!', 'dfri' ) );

if ( post_password_required() ) { ?>
<section id="comments">
	<div class="notice">
		<p class="bottom"><?php esc_html_e( 'This post is password protected. Enter the password to view comments.', 'dfri' ); ?></p>
	</div>
</section>
<?php
	return;
}
?>

<?php
if ( comments_open() ) :
	if ( (is_page() || is_single()) && ( ! is_home() && ! is_front_page()) ) :
?>
<section id="respond">
	<h3><?php comment_form_title( esc_html__( 'Leave a Reply', 'dfri' ), esc_html__( 'Leave a Reply to %s', 'dfri' ) ); ?></h3>
	<p class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></p>
	<?php if ( get_option( 'comment_registration' ) && ! is_user_logged_in() ) : ?>
	<p><?php printf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'dfri' ), esc_url( wp_login_url( get_permalink() ) ) ); ?></p>
	<?php else : ?>
	<form action="<?php echo esc_url( get_option( 'siteurl' ) ); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( is_user_logged_in() ) : ?>
		<p><?php printf( __( 'Logged in as <a href="%s/wp-admin/profile.php">%s</a>.', 'dfri' ), esc_url( get_option( 'siteurl' ) ), esc_html( $user_identity ) ); ?> <a href="<?php echo esc_url( wp_logout_url( get_permalink() ) ); ?>" title="<?php esc_attr__( 'Log out of this account', 'dfri' ); ?>"><?php esc_html_e( 'Log out &raquo;', 'dfri' ); ?></a></p>
		<?php else : ?>
		<p>
			<label for="author"><?php esc_html_e( 'Name', 'dfri' ); if ( $req ) { esc_html_e( ' (required)', 'dfri' ); } ?></label>
			<input type="text" class="five" name="author" id="author" value="<?php echo esc_attr( $comment_author ); ?>" size="22" <?php if ( $req ) { echo "aria-required='true'"; } ?>>
		</p>
		<p>
			<label for="email"><?php esc_html_e( 'Email (will not be published)', 'dfri' ); if ( $req ) { esc_html_e( ' (required)', 'dfri' ); } ?></label>
			<input type="text" class="five" name="email" id="email" value="<?php echo esc_attr( $comment_author_email ); ?>" size="22" <?php if ( $req ) { echo "aria-required='true'"; } ?>>
		</p>
		<p>
			<label for="url"><?php esc_html_e( 'Website', 'dfri' ); ?></label>
			<input type="text" class="five" name="url" id="url" value="<?php echo esc_attr( $comment_author_url ); ?>" size="22">
		</p>
		<?php endif; ?>
		<p>
			<label for="comment"><?php esc_html_e( 'Comment', 'dfri' ); ?></label>
			<textarea name="comment" id="comment"></textarea>
		</p>
		<p id="allowed_tags" class="small"><strong>XHTML:</strong> <?php esc_html_e( 'You can use these tags:','dfri' ); ?> <code><?php echo allowed_tags(); ?></code></p>
		<p><input name="submit" class="button" type="submit" id="submit" value="<?php esc_attr_e( 'Submit Comment', 'dfri' ); ?>"></p>
		<?php comment_id_fields(); ?>
		<?php do_action( 'comment_form', $post->ID ); ?>
	</form>
	<?php endif; // If registration required and not logged in ?>
</section>
<?php
	endif; // if you delete this the sky will fall on your head
endif; // if you delete this the sky will fall on your head
?>
