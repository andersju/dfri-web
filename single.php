<?php get_header(); ?>
<div class="row">
	<div class="small-12 large-9 columns" role="main">

	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<?php get_template_part( 'parts/post-meta' ); ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">

			<?php if ( has_post_thumbnail() ) : ?>
				<div class="row">
					<div class="column">
						<?php the_post_thumbnail( '', array( 'class' => 'th' ) ); ?>
					</div>
				</div>
			<?php endif; ?>

			<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages( array( 'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'dfri' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php get_template_part( 'parts/post-tags' ); ?></p>
			</footer>
			<?php comments_template(); ?>
		</article>
	<?php endwhile;?>

	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
