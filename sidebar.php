<aside id="sidebar" class="small-12 large-3 columns">
	<div class="hide-for-large-up">
		<?php get_search_form( true ); ?>
	</div>

	<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
</aside>
