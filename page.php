<?php get_header(); ?>

<div class="row">
	<div class="small-12 large-9 columns" role="main">

	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>

			<?php
				$children = wp_list_pages( 'title_li=&child_of='.$post->ID.'&echo=0&depth=2' );
				if ( $children ) {
					printf( '<h3>%s</h3><ul>%s</ul>', esc_html__( 'Subpages', 'dfri' ), $children );
				}
			?>

			<footer>
				<?php wp_link_pages( array( 'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'dfri' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php comments_template(); ?>
		</article>
	<?php endwhile;?>

	</div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
