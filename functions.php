<?php
/*
Author: Ole Fredrik Lie
URL: http://olefredrik.com
*/

if ( ! defined( 'DFRI2_VERSION' ) ) {
	define( 'DFRI2_VERSION', '20150521' );
}

// Various clean up functions
require_once( 'library/cleanup.php' );

// Required for Foundation to work properly
require_once( 'library/foundation.php' );

// Register all navigation menus
require_once( 'library/navigation.php' );

// Add menu walkers
require_once( 'library/menu-walker.php' );
require_once( 'library/offcanvas-walker.php' );

// Create widget areas in sidebar and footer
require_once( 'library/widget-areas.php' );

// Enqueue scripts
require_once( 'library/enqueue-scripts.php' );

// Add theme support
require_once( 'library/theme-support.php' );

// Add Header image
require_once( 'library/custom-header.php' );

// Remove old qtranslate cookies to fix issue with language switcher
if ( isset( $_COOKIE['qtrans_front_language'] ) ) {
    unset( $_COOKIE['qtrans_front_language'] );
    setcookie( 'qtrans_front_language', '', time() - 3600, '/' );
}

?>
